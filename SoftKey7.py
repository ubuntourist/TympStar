#!/usr/bin/env python
# TITLE:   SoftKey6.py
# AUTHOR:  Kevin Cole, Gallaudet University <kjcole@gri.gallaudet.edu>
# LASTMOD: 2003.10.06
# PURPOSE: A simple test for talking to the TympStar.  This should
#          send a "Menu Softkey 6", which ususally corresponds to "More"
#          The result should be a change in the menu at the bottom of
#          the TympStar display.
# NOTES:   See "Python Programming on Win32" pages 373-379.  And note
#          that "send" is now "write".  See also "import pydoc",
#          "pydoc.doc(Serial)" for more specific info...

CRLF = '\015\012'
SOR = '|'               # Start of Record
RT  = '001'             # Record type
RC  = '0'               # Request code = 'send'
FC  = '05'              # Function code = 'Softkey 6'
CR  = '\015'            # Carriage Return
LF  = '\012'            # Line Feed

import serial
import serial.serialutil
from time import sleep
from string import *
from win32com.client import Dispatch

wire = serial.Serial(port=0, 
              baudrate=19200,
              bytesize=serial.SEVENBITS,
              stopbits=serial.STOPBITS_TWO,
              parity=serial.PARITY_EVEN,
              timeout=None)
print wire.portstr      # Print which port it actually is

for i in range(10):
  wire.setRTS(1)
  #wire.write('|001005'+CRLF)       # Send TympStar command "Menu Key 6"
  wire.write(SOR+RT+RC+FC+CR+LF)    # Simulate Softkey 6 press
  sleep(1)

facility = "Gallaudet University, Audiology Department"
patient = strip(raw_input("Enter patient name (45 char max):"))
tester  = strip(raw_input("Enter tester  name (45 char max):"))

#========
sleep(1)
wire.write("|001008\015\012")            # ETF
sleep(1)
wire.write("|001013\015\012")            # RETURN
sleep(1)
wire.write("|001002\015\012")            # Instrument Options
sleep(1)
wire.write("|001005\015\012")            # More
sleep(1)
wire.write("|001001\015\012")            # Facility Name
sleep(1)

#wire.write("|115021")                    # YYYY/MM/DD 24 hour clock
#wire.write("|11600%4.4s"   % (mm))       # Set Month
#wire.write("|11601%4.4s"   % (dd))       # Set Day
#wire.write("|11602%4.4s"   % (yyyy))     # Set Year
#wire.write("|11700%2.2s"   % (hh))       # Set Hour
#wire.write("|11701%2.2s"   % (nn))       # Set Minute
wire.write("|1050%-45.45s\015\012" % (facility)) # Set Facility
sleep(1)
wire.write("|001005\015\012")            # Store
sleep(1)
wire.write("|001006\015\012")            # TYMP
#========
sleep(1)
wire.write("|1020%-45.45s\015\012" % (patient))
sleep(1)
wire.write("|1040%-45.45s\015\012" % (tester))
sleep(1)
wire.write("|1021%45.45s\015\012" % " ")
sleep(1)
print "Patient: %s" % (wire.readline())
sleep(1)
wire.write("|1200\015\012")
sleep(1)
wire.write("|1041%45.45s\015\012" % " ")
sleep(1)
print "Tester:  %s" % (wire.readline())
sleep(1)
wire.write("|1200\015\012")
sleep(1)
wire.close()

xlApp = Dispatch("Excel.Application")
xlApp.Workbooks.Add()
xlBook = xlApp.Workbooks(1)
xlSheet = xlApp.Sheets(1)
xlSheet.Cells(1,1).Value = patient
xlSheet.Cells(2,1).Value = tester
xlBook.SaveAs(Filename="C:\\Documents and Settings\\Wendy Hanks\\My Documents\\Code\\TympStar.xls")
xlApp.Visible = 1
