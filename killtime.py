#!/usr/bin/env python
#########################################################################
#    NAME: killtime.py
#  AUTHOR: Kevin Cole <kjcole@gri.gallaudet.edu>
#   WHERE: Gallaudet Research Institute, Washington, DC 20002
# LASTMOD: 2001.10.09
# PURPOSE: This module handles time stuff that I use a lot.
#   USAGE: from killtime import *
#
# Copyright (c) 2001 Kevin Cole <kjcole@gri.gallaudet.edu>
#########################################################################

from time	import *	# "Killing time????" said Tick, the WatchDog.
from calendar	import isleap

# NOTE: Because lists start with element 0, dpm, mmm and month have a dummy 
# first element of the list.  (It's either that, or always remember to 
# subtract one from the month returned by the OS.) On the other hand, 
# Monday thru Sunday are returned from the OS as 0 through 6, so no need 
# to have a dummy www[0] or week[0] element.  Just remember that the week
# starts on a Monday.

# (Three of the lines below might better be thought of as a class "Months"
# with attributes/characteristics/properties dpm,abbr,name)  Maybe I'll
# do that someday.

dpm   = [0,31,28,31,30,31,30,31,31,30,31,30,31]   # dpm = Days Per Month
mmm   = ["","Jan","Feb","Mar","Apr","May","Jun",
         "Jul","Aug","Sep","Oct","Nov","Dec"]
month = ["","January","February","March","April","May","June","July",
         "August","September","October","November","December"]
www   = ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"]
week  = ["Monday","Tuesday","Wednesday","Thursday",
         "Friday","Saturday","Sunday"]

# Get the current time (year, month, day, hour, minute, second,
# weekday, Julian day and Daylight Savings Time (DST) boolean flag) and
# massage it a few times.

today     = localtime(time())    # (yyyy,mm,dd,hh,mm,ss,wd,jd,DST)
yyyy,mm,dd,hh,nn,ss,wd,jd,DST  = today
right_now = strftime("%a %b %d %H:%M:%S %%s %Y",today)  % (tzname[DST])
created   = strftime("%a. %d %b %Y %H:%M:%S %%s",today) % (tzname[DST])
next_year = localtime(mktime((yyyy+1,)+(today[1:])))
expires   = strftime("expires=%A, %d-%b-%Y 23:59:59 %%s",next_year) % (tzname[DST])

dpm[2] = dpm[2] + isleap(yyyy)	# Take care of the leap day.

#########################################################################
# whatday returns the day of the week (0=Monday, 6=Sunday) when fed	#
# a date in the form yyyy,mm,dd						#
#########################################################################

def whatday(yyyy,mm,dd):
  a = (14 - mm) / 12
  y = yyyy - a
  m = mm + 12*a - 2
  d = (5 + dd + y + y/4 + (31*m)/12) % 7
  return d

#########################################################################
# whatdate returns the day of the month (1 - 31) when for the Nth	#
# occurrance of weekday (0 - 6)						#
#########################################################################

def whatdate(yyyy,mm,Nth,weekday):
  dd = (1 + (Nth-1)*7 + (weekday - whatday(yyyy,mm,1)) % 7)
  if (dd > dpm[mm]):
    dd = None
  return dd
