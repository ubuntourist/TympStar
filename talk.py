#!/usr/bin/env python
# Written by Kevin Cole <kjcole@gri.gallaudet.edu> 2002.10.05
# From an example in the book Python Programming for Win32
# by Mark Hammond and Andy Robinson

# Note: Install Roger Burnham's SioModule.  In order to do this,
# you may have to add a line to the Registry as follows:
#
#   \HKEY_LOCAL_MACHINE\SOFTWARE\Python\PythonCore\2.2\InstallPath
#   Set the Default value to "C:\Python22" (or whatever the top-level
#   Python install directory is.
#

from Serial import Serial
from sys import stdout
myconfig = Serial.PortDict()    # Create a dictionary with port settings
myconfig["port"] = Serial.COM1  # Change the default port
myport = Serial.Port(myconfig)
myport.open()
myport.write("ATI\015")         # BOOK TYPO: Use write() not send()
#print myport.read()
#print myport.read(1024, timed=1)

while True:
  stdout.write(myport.read(1,timed=1))
print myconfig
myport.close()

