#!/usr/bin/env python
#########################################################################
# Module:	Array							#
# Author:	Kevin Cole <kjcole@gri.gallaudet.edu>			#
# Modified:	00.02.26						#
# Description:	A more FORTRAN-like array, using the built-in list type	#
#		as a base.  By default the first element of the list is	#
#		x[1].  However, the list can be defined as starting at	#
#		any integer, by calling the class with a lower bound	#
#		as the first argument.					#
#########################################################################

"""Simulate FORTRAN arrays using lists.

A more FORTRAN-like array, using the built-in list type as a base. By
default the first element of the Array list is referenced with an
index value of [1].  However, the Array list can be defined as starting
at any integer, by calling the class with a lower bound as the first
argument."""

from UserList import *

class Array(UserList):				# Base on UserList wrapper
  def __init__(self, lowerBound=1, list=None):	# Lower bound is 1 by default
    self.offset  = lowerBound			# Per instance lower bound
    self.data = []				# Per instance empty list
    if list is not None:			# If initial values supplied,
      if type(list) == type(self.data):		# ...determine type, and...
        self.data[:] = list			# ...fill appropriately.
      else:
        self.data[:] = list.data[:]

  def __getitem__(self, subscript):		# Don't index normally
    if (subscript - self.offset) < 0:		# Don't allow negative indices
      raise IndexError, "list index out of range"
    else:
      return self.data[subscript - self.offset]	# Adjust index by offset

  def __setitem__(self, subscript, item):	# Don't index normally
    if (subscript - self.offset) < 0:		# Don't allow negative indices
      raise IndexError, "list index out of range"
    else:
      self.data[subscript - self.offset] = item	# Adjust index by offset

  def __delitem__(self, subscript):		# Don't index normally
    if (subscript - self.offset) < 0:		# Don't allow negative indices
      raise IndexError, "list index out of range"
    else:
      del self.data[subscript - self.offset]	# Adjust index by offset

  def __getslice__(self, low, high):
    if (low  - self.offset) < 0 or \
       (high - self.offset) < 0:
      raise IndexError, "slice index out of range"
    elif (low > high):
      raise IndexError, "lower bound greater than higher bound"
    else:
      return self.data[low - self.offset:high - self.offset]

