#!/usr/bin/env python
from string   import *
from TympStar import *

filename = strip(raw_input("File: "))
log = open(filename,"r")	# Data source
csv = open("Plot.csv","w")	# For Excel:    X,X,X,X\n  Y,Y,Y,Y\n
dat = open("Plot.dat","w")      # For plotting: X Y\n  X Y\n

buffer = []                   # Create an empty buffer
done = 0

while not done:               # Until the done flag is set...
  record = log.readline()     # ...Read a record
  resp   = Response(record)   # ...Check and parse it again
  while not resp.valid:       # ...Loop til we get valid record
    print "NAK"               # ......Send NAK
    record = log.readline()   # ......Read it again
    resp   = Response(record) # ......Check and parse it again
  print "ACK"                 # ...Send ACK
# buffer.append(resp.data)    # ...Accumulate data
  buffer.append(record)       # ...Accumulate data
  if resp.rectype == "3":     # ...If End of XY Data...
    done = 1                  # ......We're done

print "="*72
for line in buffer:
  print line
  resp = Response(line)
  if resp.rectype == "2":
    print resp.L1XY
    for XY in resp.L1XY:
      print "%s" % (XY[0])
      dat.write("%s %s\n" % (XY[0], XY[1]))
      csv.write("%s," % (XY[0]))

csv.write("\n")

for line in buffer:
  print line
  resp = Response(line)
  if resp.rectype == "2":
    print resp.L1XY
    for XY in resp.L1XY:
      print "%s," % (XY[1])
      csv.write("%s," % (XY[1]))

csv.write("\n")
print "="*72

log.close()
csv.close()
dat.close()
