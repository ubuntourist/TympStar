#!/usr/bin/env python

# Written by Kevin Cole <kjcole@gri.gallaudet.edu> 2002.10.25

# This is based on talk.py which in turn is based on an
# an example in the book Python Programming for Win32
# by Mark Hammond and Andy Robinson

# Note: Install Roger Burnham's SioModule.  In order to do this,
# you may have to add a line to the Registry as follows:
#
#   \HKEY_LOCAL_MACHINE\SOFTWARE\Python\PythonCore\2.2\InstallPath
#   Set the Default value to "C:\Python22" (or whatever the top-level
#   Python install directory is.
#

from Serial import Serial
from sys import stdout

CRLF = "\015\012"   # Ctrl-M, Ctrl-J

myconfig = Serial.PortDict()    # Create a dictionary with port settings
myconfig["port"] = Serial.COM1  # Change the default port
myconfig["baud"] = Serial.Baud19200        # Set baud to highest GSI rate
myconfig["dataBits"] = Serial.WordLength7
myconfig["stopBits"] = Serial.TwoStopBits
myconfig["parity"] = Serial.EvenParity
myport = Serial.Port(myconfig)
myport.open()

# BOOK TYPO: Use write() not send()
myport.write("I001005%s" % (CRLF))  # Send a "More" command (Soft Key 6)

#print myport.read()
#print myport.read(1024, timed=1)
#while True:
#  stdout.write(myport.read(1,timed=1))

buffer = myport.read(1024, timed=0)
print buffer

myport.close()

