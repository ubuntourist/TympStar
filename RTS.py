#!/usr/bin/env python
from time import sleep
from TympStar import *
GSI = TympStar()
for i in range(100):
  RTS = 1
  GSI.setRTS(RTS)
  print "RTS = %s   CTS = %s" % (RTS, GSI.getCTS())
  sleep(5)
  RTS = 0
  GSI.setRTS(RTS)
  print "RTS = %s   CTS = %s" % (RTS, GSI.getCTS())
  sleep(5)

