#!/usr/bin/env python
# TITLE:   Collect.py
# AUTHOR:  Kevin Cole, Gallaudet University <kjcole@gri.gallaudet.edu>
# LASTMOD: 2003.10.23
# PURPOSE: To extend the serial class to include TympStar GTI attributes
#          and methods.
# NOTES:   See "Python Programming on Win32" pages 373-379.  And note
#          that "send" is now "write".  See also "import pydoc",
#          "pydoc.doc(Serial)" for more specific info...
#
#          Further analysis leads me to suspect pySerial may be better
#          than what I've chosen below.  pySerial appears to have the
#          ability to raise and lower RTS, which means we can turn hand-
#          shake back on...
#
# Communications: COM1, 19200 baud, 2 stop bits, even parity, no handshake
#
# Set RECORD TYPE to SUMMARY or to SUMMARY + XY (graphics)
# Set CLEAR TEST to AUTO or MANUAL
# Set DATA FORMAT to GSI TYMPSTAR DATA (not GSI 33 DATA)

from TympStar import *
from win32com.client import Dispatch

DEBUG = 0              # Not debugging at the moment

log = open("TympStar.log","w")
GSI = TympStar()       # Open a line to the GSI TympStar

facility = "Gallaudet University, Audiology Department"
patient = strip(raw_input("Enter patient name (45 char max):"))
tester  = strip(raw_input("Enter tester  name (45 char max):"))

GSI.send("001006")                     # TYMP
GSI.send("1020%-45.45s" % (patient))   # Set Patient Name
GSI.send("1040%-45.45s" % (tester))    # Set Tester Name

#################################################################

tests  = int(strip(raw_input("How many tests (1-29): ")))
GSI.send("001014")              # PAGE
GSI.send("1190001")             # Transmit all tests

for test in range(tests):         # For each test...
  buffer = []                     # ...Create an empty buffer
  done = 0                        # ...Not done til the fat lady sings.
  while not done:                 # ...Until the done flag is set...
    record = GSI.readline()       # ......Read a record
    resp   = Response(record)     # ......Check and parse it again
    while not resp.valid:         # ......Loop til we get valid record
      GSI.send("1210")            # .........Send NAK
      record = GSI.readline()     # .........Read it again
      resp   = Response(record)   # .........Check and parse it again
    GSI.send("1200")              # ......Send ACK
    buffer.append(record)         # ......Accumulate data
    if resp.rectype == "3":       # ......If End of XY Data...
      done = 1                    # .........We're done with the a test.
  
  for line in buffer:             # ...For one set of test results...
    log.write(line)               # ......write the buffer.

  del buffer                      # ...Empty the buffer for the next test

del buffer
GSI.close()
log.close()

#==================================

#filename = strip(raw_input("File: "))
log = open("TympStar.log","r")	# Data source
csv = open("Plot.csv","w")	# Excel format:   "X,X,X...\n" "Y,Y,Y...\n"
dat = open("Plot.dat","w")      # GNUplot format: "X Y\n" "X Y\n" ...

buffer = []                   # Create an empty buffer
done = 0

while not done:               # Until the done flag is set...
  record = log.readline()     # ...Read a record
  resp   = Response(record)   # ...Check and parse it again
  buffer.append(record)       # ...Accumulate data
  if resp.rectype == "3":     # ...If End of XY Data...
    done = 1                  # ......We're done

for line in buffer:
  resp = Response(line)                      # Decode response record
  if resp.rectype == "2":                    # If resp is (X,Y) pairs...
    for X in resp.L1Xaxis:                   # ...For each X on the X axis...
      cvs.write("%s," % (X))                 # ......Append X to line in csv
      dat.write("%s %s\n" % (X, resp.L1[X])) # ......Write "X Y" line to dat
csv.write("\n")                              # End csv line with a CRLF

for line in buffer:
  resp = Response(line)                      # Decode response record
  if resp.rectype == "2":                    # If resp is (X,Y) pairs...
    for X in resp.L1Xaxis:                   # ...For each X on the X axis...
      csv.write("%s," % (resp.L1[X]))        # ......Append Y to line in csv
csv.write("\n")                              # End csv line with a CRLF

#for line in buffer:
#  resp = Response(line)
#  if resp.rectype == "2":
#    for XY in resp.L1XY:
#      csv.write("%s," % (XY[0]))            # Bunch o' X values
#      dat.write("%s %s\n" % (XY[0], XY[1])) # X Y
#csv.write("\n")                             # Don't forget the CRLF

#for line in buffer:
#  resp = Response(line)
#  if resp.rectype == "2":
#    for XY in resp.L1XY:
#      csv.write("%s," % (XY[1]))            # Bunch o' Y values
#csv.write("\n")                             # Don't forget the CRLF

log.close()
csv.close()
dat.close()

xlApp = Dispatch("Excel.Application")
xlApp.Workbooks.Add()
xlBook = xlApp.Workbooks(1)
xlSheet = xlApp.Sheets(1)
xlSheet.Cells(1,1).Value = patient
xlSheet.Cells(2,1).Value = tester
xlBook.SaveAs(Filename="C:\\Documents and Settings\\Wendy Hanks\\My Documents\\Code\\TympStar.xls")
xlApp.Visible = 1
