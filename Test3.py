#!/usr/bin/env python
from TympStar import *
from win32com.client import Dispatch

DEBUG = 0              # Not debugging at the moment

log = open("TympStar.log","w")
GSI = TympStar()       # Open a line to the GSI TympStar

RT  = GSI.command["Key Press"]  # Record type
RC  = GSI.setget["Set"]         # Request code
FC  = GSI.funky["SOFTKEY6"]     # Function keycode

#for i in range(10):
#  GSI.setRTS(1)
#  #GSI.send("|001005"+CRLF)       # Send TympStar command "Menu Key 6"
#  GSI.send(SOR+RT+RC+FC+CR+LF)    # Simulate Softkey 6 press

facility = "Gallaudet University, Audiology Department"
patient = strip(raw_input("Enter patient name (45 char max):"))
tester  = strip(raw_input("Enter tester  name (45 char max):"))

GSI.send("001008")                     # ETF
GSI.send("001013")                     # RETURN
GSI.send("001002")                     # Instrument Options
GSI.send("001005")                     # More
GSI.send("001001")                     # Facility Name
#GSI.send("115021")                    # YYYY/MM/DD 24 hour clock
#GSI.send("11600%4.4s"   % (mm))       # Set Month
#GSI.send("11601%4.4s"   % (dd))       # Set Day
#GSI.send("11602%4.4s"   % (yyyy))     # Set Year
#GSI.send("11700%2.2s"   % (hh))       # Set Hour
#GSI.send("11701%2.2s"   % (nn))       # Set Minute
GSI.send("1050%-45.45s" % (facility))  # Set Facility Name
GSI.send("001005")                     # Store
GSI.send("001006")                     # TYMP
GSI.send("1020%-45.45s" % (patient))   # Set Patient Name
GSI.send("1040%-45.45s" % (tester))    # Set Tester Name

#################################################################

if DEBUG:
  print GSI.portstr                      # Print I/O port name
  GSI.send("1021%45.45s" % " ")          # Get Patient Name
  feedback = GSI.readline()
  print "Patient:  %s" % (feedback)
  log.write(feedback)
  GSI.send("1200")                       # Acknowledge
  GSI.send("1041%45.45s" % " ")          # Get Tester Name
  feedback = GSI.readline()
  print "Tester:   %s" % (feedback)
  log.write(feedback)
  GSI.send("1200")                       # Acknowledge
  GSI.send("001008")                     # ETF
  GSI.send("001013")                     # RETURN
  GSI.send("001002")                     # Instrument Options
  GSI.send("001005")                     # More
  GSI.send("001001")                     # Facility Name
  GSI.send("1051%45.45s" % " ")          # Get Facility Name
  feedback = GSI.readline()
  print "Facility: %s" % (feedback)
  log.write(feedback)
  GSI.send("1200")                       # Acknowledge
  GSI.send("001005")                     # Store
  GSI.send("001006")                     # TYMP

#################################################################
#GSI.send("1190001") # Transmit all tests
#
#
#  GSI.send("1200")
#################################################################

buffer = []                   # Create an empty buffer
record = " "*10 + GSI.CRLF    # Create an invalid record
resp   = Response(record)     # Parse it to get record type
while resp.rectype != 3:      # Until we get an End of XY data
  while not resp.valid:       # ...Loop til we get valid record
    GSI.send("1210")          # ......Send NAK
    record = GSI.readline()   # ......Read it again
    resp   = Response(record) # ......Check and parse it again
  GSI.send("1200")            # ...Send ACK
  buffer.append(resp.data)    # ...Accumulate data

for line in buffer:
  log.write(line)

GSI.close()
log.close()

#xlApp = Dispatch("Excel.Application")
#xlApp.Workbooks.Add()
#xlBook = xlApp.Workbooks(1)
#xlSheet = xlApp.Sheets(1)
#xlSheet.Cells(1,1).Value = patient
#xlSheet.Cells(2,1).Value = tester
#xlBook.SaveAs(Filename="C:\\Documents and Settings\\Wendy Hanks\\My Documents\\Code\\TympStar.xls")
#xlApp.Visible = 1
