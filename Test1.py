#!/usr/bin/env python
from string import *

SOR = "|"               # Start of Record
CR  = "\015"            # Carriage Return
LF  = "\012"            # Line Feed
CRLF = CR+LF            # Carriage Return, Line Feed
fmt  = SOR+"%s"+CRLF    # Standard command format

command = {"Key Press"    : "001",
           "Patient info" : "102",
           "Tester Info"  : "104",
           "Send ACK"     : "120",
           "Send NAK"     : "121"}

setget = {"Set" : "0", "Get" : "1"}     # Send or Query

ACK = "|1200" + CRLF
NAK = "|1210" + CRLF

def check(line):
  line = strip(line)            # Remove CRLF
  data = line[:-2]              # Everything up to the last two characters
  sent = lower("0x"+line[-2:])  # The last two characters
  sum = 0
  for i in range(len(data)):
    sum += ord(data[i])        # add ASCII arithmetic value to sum
  calc = "0x%02x" % (sum % 256)
  if (sent == calc) and \
     (data[0:1] == SOR):        # If checksums match and record start ok...
    print "Checksum OK"         # ...all is well
#   print ACK                   # ...send an ACK to the TympStar
  else:                         # Otherwise...
    print "Checksum FAILED... retrying"
    print "Given: %s   Computed: %s" % (sent, calc)
#   print NAK                   # ...send an NAK to the TympStar

#for line in open("Test.log","r").readlines():
#  check(line)

filename = strip(raw_input("File: "))
for line in open(filename,"r").readlines():
  check(line)
