#!/usr/bin/env python
# TITLE:   Test9.py
# AUTHOR:  Kevin Cole, Gallaudet University <kjcole@gri.gallaudet.edu>
# LASTMOD: 2003.12.08
# PURPOSE: To plot multiple lines.

from TympStar import *

DEBUG = 0              # Not debugging at the moment
title = 'set title "Tymp Diagnostic %s\\n\\nPATIENT: %s    TESTER: %s"\n'
plot  = 'plot [%s:%s] "T%s.dat" title "Test %s" with lines\n'

filename = strip(raw_input('File: '))
log = open(filename,'r')	# Data source
csv = open('Plot.csv','w')	# Excel format:   'X,X,X...\n' 'Y,Y,Y...\n'
gpl = open('gnuplot.dem','w')   # GNUplot command file

gpl.write('# To run from the shell:\n\n')
gpl.write('#    gnuplot gnuplot.dem\n')
gpl.write('#    ps2pdf Plot.ps\n\n')
gpl.write('#plot "Plot.dat" with lines\n')
gpl.write('#pause -1 "Hit return to continue"\n')
gpl.write('#set terminal postscript landscape monochrome solid\n')
gpl.write('set  terminal postscript landscape color      solid\n')
gpl.write('set output "Plot.ps"\n')
gpl.write('set title "Tymp Diagnostic"\n')
gpl.write('set xlabel "Pressure (daPa)"\n')
gpl.write('set ylabel "Compliance (ml/mmho * 1000)"\n\n')

test = 1

for record in log.readlines():            # For each record...
  r = Response(record)                    #   Parse it apart
  if r.rectype == '8':                    #   If it's starting new test...
    gpl.write(title % (r.dateTime[:-3],r.patientName,r.testerName))
    dat = open('T%s.dat' % (test),'w')    #     Open new GNUplot data file
    if r.numLines > 0: L1 = {}            #     Make a points dictionary for L1
    if r.numLines > 1: L2 = {}            #     Make a points dictionary for L2
    if r.numLines > 2: L3 = {}            #     Make a points dictionary for L3
  elif r.rectype == '2':                  #   If it's (X,Y) pairs...
    for X in r.L1Xaxis:                   #     For each X on the X axis...
      L1[X] = r.L1[X]                     #       Combine L1 dictionaries
  elif r.rectype == '3':                  #   If no more data for this test...
    L1Xaxis = L1.keys()                   #     Get the completed X axis
    L1Xaxis.sort()                        #     Put it in ascending order
    for X in L1Xaxis:                     #     For each X on the X axis...
      csv.write('%s,' % (X))              #       Append X to line in csv
      dat.write('%s %s\n' % (X, L1[X]))   #       Write 'X Y' line to dat
    csv.write('\n')                       #     End csv line with a CRLF
    for X in L1Xaxis:                     #     For each X on the X axis...
      csv.write('%s,' % (L1[X]))          #       Append Y to line in csv
    csv.write('\n')                       #     End csv line with a CRLF
    dat.close()
#   gpl.write(plot % (L1Xaxis[0], L1Xaxis[-1], test, test))
    gpl.write(plot % (-420, 220, test, test))
    test += 1

gpl.write('#pause -1 "Hit return to continue"\n')
gpl.write('reset\n')

log.close()
csv.close()
gpl.close()
