Captian's Log. Stardate 2003.10.15
As recoreded by Kevin Cole <kjcole@gri.gallaudet.edu>

2003.10.15
	Hyperterm settings
	  COM1
	    Baud: 19200
	    Data Bits: 7
	    Parity: Even
	    Stop Bits: 2
	    Flow Control: Hardware
	  ASCII Setup (under Settings)
	    Send line ends with line feeds
	    Echo characters locally

	GSI TympStar settings
	  Etf
	  Return
	  Instrument Options
	  Remote Settings

	Hit REMOTE on TympStar to start conversing

	VERTICAL BAR "|" (not lowercase "l" or upper case "I") 
	starts commands.
	Test: Send "|001005"
	Result: Change menu bar at bottom of display

2003.11.01
	These are the tests we want to perform...
	TympStar settings/sequence:

                00302   Set TYMP Diag Probe Hz       to 1000 Hz
                00400   Set TYMP Diag Admittance     to Y
                00901   Set TYMP Diag Baseline       to OFF
                01002   Set TYMP Diag Gradient       to OFF
                00500   Set TYMP Diag Pressure Range to Normal
                00803   Set TYMP Diag Start Pressure to +200 daPa
                00702   Set TYMP Diag Pressure Rate  to 200 daPa/s
          test ........................................................
                00703   Set TYMP Diag Pressure Rate  to 600/200 daPa/s
          test ........................................................
                0060    Set TYMP Diag Ear Toggle
    switch ear ........................................................
                00702   Set TYMP Diag Pressure Rate  to 200 daPa/s
          test ........................................................
                00703   Set TYMP Diag Pressure Rate  to 600/200 daPa/s
          test ........................................................
                1190001 Transmit all tests (should be 4 tests)
                107000  Clear all tests

2003.11.25
	Rodney Bolman, GSI TympStar Engineer (800) 700-2282 x2334

	OR send e-mail to <gsiservice@grasonstadler.com> or send a FAX
	608-441-2247 (neither the e-mail or FAX number are necessarily
	Rodney.)

	RTS.py sets RTS high and waits 5 seconds.  Then it sets RTS low
	and again waits 5 seconds.  Using a very simple 25-pin breakout 
	box called a "Inmac(TM) EIA QuickCheck" (Part Number 322-1), to
	monitor the cable, I see:

                         Always Hi --  2 -- Always Lo
                         Always Hi --  3 -- Always Lo
                   Alternate Hi/Lo --  4 -- Alternate Lo/Hi
   Computer 9->25        Always Lo --  5 -- Always Hi        25->9 GSI
                                     ...
                         Always Lo -- 20 -- Always Hi

	When Line 4 goes HI on the computer side, it goes LO on the GSI
	side and vice-versa.

	Using Fred's inverter, switched the behavior of lines 4 and 5.
	His other adaptor moved things all over the place... I think it
	moved the blink to Carrier Detect, but I'd have to recheck.

	A 9-pin to 25-pin chart shows Pin 7 (RTS) on a 9-pin corresponds 
	to Pin 4 (RTS) on a 25-pin and Pin 8 (CTS) on a 9-pin corresponds
	to Pin 5 (CTS) on a 25-pin.

	BUG: When the time format is YYYY/MM/DD 24:00, the three char-
	acters following the time, are NOT "---", " am" or " pm" but 
	rather "NUL ! :" aka "^@!:" or "\000!:"

	NOTE: Change the TympDiag routine.  Determine record length FIRST.
	If there's only one line, then it cuts off at 264 (plus 2 chars
	for checksum).

2003.12.01
	Running Test6.py, which does a PAGE command, followed by a
	Transmit All Tests.  Data appears to transmit correctly,
	but at the end of a data transmission, a screen appears:

	    CONTROL PROCESSOR: 1901 - 0

	    Please use the softkeys below in the following manner:
	    PRINT HELP MSG prints a description of the error and
	    the keystroke listing as a troubleshooting aid.
	    RESUME restarts the system saving any tests performed.

	    PRINT				  RESUME
	    HELP MSG

	Printing the results (for two different runs) yielded:

	    ----------------------------------------------
	      Gallaudet University, Audiology Department

	      GSI TYMPSTAR MIDDLE EAR ANALYZER
	      2003/12/01 15:11

	      CONTROL PROCESSOR: 1901 -    0

	      ERROR DATE: 12/01/2003
	      ERROR TIME: 15:11:29

	              KEY   MODE STATE  REMOTE
	      KEY  1: PAGE    2    1    0    0
	      KEY  2: RETRN   2    1    0    0
	      KEY  3: RETRN   2    1    0    0
	      KEY  4: TYMP    2    1    0    0
	      KEY  5: TYMP    2    1    0    0
	      KEY  6: TYMP    2    1    0    0
	      KEY  7: PAGE    2    1    0    0
	      KEY  8: DXFER   2    1    0    0
	      KEY  9: SK1     2    0    0    0
	      KEY 10: SK1     2    1    0    0
	    ----------------------------------------------

	    ----------------------------------------------
	      Gallaudet University, Audiology Department

	      GSI TYMPSTAR MIDDLE EAR ANALYZER
	      2003/12/01 16:25

	      CONTROL PROCESSOR: 1901 -    0

	      ERROR DATE: 12/01/2003
	      ERROR TIME: 16:25:33

	              KEY   MODE STATE  REMOTE
	      KEY  1: PAGE    2    1    0    0
	      KEY  2: TYMP    2    1    0    0
	      KEY  3: REMOT   2    1    0    0
	      KEY  4: ETF     2    1    0    0
	      KEY  5: ETF     2    1    0    0
	      KEY  6: ETF     2    1    0    0
	      KEY  7: RETRN   2    1    0    0
	      KEY  8: ETF     2    1    0    0
	      KEY  9: REMOT   2    1    0    0
	      KEY 10: TYMP    2    1    0    0
	    ----------------------------------------------

	Recommendations from both Flint and Kremer: Slow it down to
	300 baud.  Flint also suggests using Linux's minicom, and
	furthermore hints that the problem may be the TympStar clock:
	If it cannot figure out the time exactly, it would throw off
	the timing of the X sampling, and quite possibly also the
	display of the AM/PM in the summary record.
