# Project TympStar

Ancient code from late 2003, unearthed because, in late 2020, I need
to get back to low-level serial code in Python, and assume my old
examples will be the best way to get me there.

The GSI TympStar was a device that measured middle ear pressure over
time, and displayed the results on a tiny LCD screen. It had no
internal memory, but it did have an RS-232 serial port hanging off of
the back of it.

This mess 'o code retrieved data from the TympStar and stored it in
files that could later be imported into spreadsheets. Created in a time
before I used version control, and with no particular testing technique,
it will probably take some study, and clean up to comprehend. 

But, for now, the goal is to put it somewhere that I won't lose it.

The most likely place to start is `TympStarWorks.py`.

There is also an example of plotting the data with GNU Plot
(`gnuplot.dem`).

----

