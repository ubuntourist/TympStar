#!/usr/bin/env python

def hex2sint(hexstring):
  """Return 16-bit signed integer from a 4-digit hexidecimal string"""
  i = int(hexstring,16)
  if i > 32767:
    i = i - 65536
  return i

while (1 == 1):
  hex = raw_input("\nEnter a 4-digit hex number: ")
  print "Hex: %s   Unsigned: %s  Signed: %s" % (hex,int(hex,16),hex2sint(hex))
