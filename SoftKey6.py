#!/usr/bin/env python
# TITLE:   SoftKey6.py
# AUTHOR:  Kevin Cole, Gallaudet University <kjcole@gri.gallaudet.edu>
# LASTMOD: 2003.10.06
# PURPOSE: A simple test for talking to the TympStar.  This should
#          send a "Menu Softkey 6", which ususally corresponds to "More"
#          The result should be a change in the menu at the bottom of
#          the TympStar display.
# NOTES:   See "Python Programming on Win32" pages 373-379.  And note
#          that "send" is now "write".  See also "import pydoc",
#          "pydoc.doc(Serial)" for more specific info...
#
#          Further analysis leads me to suspect pySerial may be better
#          than what I've chosen below.  pySerial appears to have the
#          ability to raise and lower RTS, which means we can turn hand-
#          shake back on...
#
# Communications: COM1, 19200 baud, 2 stop bits, even parity, no handshake
#
# Set RECORD TYPE to SUMMARY or to SUMMARY + XY (graphics)
# Set CLEAR TEST to AUTO or MANUAL
# Set DATA FORMAT to GSI TYMPSTAR DATA (not GSI 33 DATA)

import serial
import serial.serialutil
from time import sleep
from string import *
from win32com.client import Dispatch
from killtime import *  # My own time routines

SOR = "|"               # Start of Record
CR  = "\015"            # Carriage Return
LF  = "\012"            # Line Feed
CRLF = CR+LF            # Carriage Return, Line Feed
fmt  = SOR+"%s"+CRLF    # Standard command format

command = {"Key Press"    : "001",
           "Patient info" : "102",
           "Tester Info"  : "104",
           "Send ACK"     : "120",
           "Send NAK"     : "121"}

setget = {"Set" : "0", "Get" : "1"}     # Send or Query

funky = {"SOFTKEY1" : "00", "SOFTKEY2" : "01", "SOFTKEY3" : "02",
         "SOFTKEY4" : "03", "SOFTKEY5" : "04", "SOFTKEY6" : "05",
         "TYMP"     : "06", "REFLEX"   : "07", "ETF"      : "08",
         "SPECIAL"  : "09", "START_R"  : "10", "START_L"  : "11",
         "ERASE"    : "12", "RETURN"   : "13", "PAGE"     : "14",
         "CLEAR"    : "15", "REMOTE"   : "16", "DATA_X"   : "17",
         "STIM_D"   : "18", "STIM_U"   : "19", "INTEN_D"  : "20",
         "INTEN_U"  : "21", "PRINT"    : "22", "UNUSED"   : "23",
         "STOP"     : "24", "HOLD"     : "25", "PRESENT"  : "26"}

ACK = "|1200" + CRLF
NAK = "|1210" + CRLF

def check(line):
  line = strip(line)            # Remove CRLF
  data = line[:-2]              # Everything up to the last two characters
  sum1 = lower("0x"+line[-2:])  # The last two characters
  sum2 = 0
  for i in range(len(data)):
    sum2 += ord(data[i])        # add ASCII arithmetic value to sum2
  if (sum1 == hex(sum2)) and \
     (data[0:1] == SOR):        # If checksums match and record start ok...
    print "Checksum OK"         # ...all is well
    myport.write(ACK)           # ...send an ACK to the TympStar
  else:                         # Otherwise...
    print "Checksum FAILED... retrying"
    myport.write(NAK)           # ...send an NAK to the TympStar

def xmit(device, command, flag, args):
  sleep(1)
  fmt = SOR+"%s"+CR+LF
  device.write(fmt % (command,flag,args))

wire = serial.Serial(port=0, 
              baudrate=19200,
              bytesize=serial.SEVENBITS,
              stopbits=serial.STOPBITS_TWO,
              parity=serial.PARITY_EVEN,
              timeout=None)
print wire.portstr      # Print which port it actually is

RT  = command["Key Press"]  # Record type
RC  = setget["Set"]         # Request code
FC  = funky["SOFTKEY6"]     # Function keycode

#for i in range(10):
#  wire.setRTS(1)
#  #wire.write("|001005"+CRLF)       # Send TympStar command "Menu Key 6"
#  wire.write(SOR+RT+RC+FC+CR+LF)    # Simulate Softkey 6 press
#  sleep(1)

facility = "Gallaudet University, Audiology Department"
patient = strip(raw_input("Enter patient name (45 char max):"))
tester  = strip(raw_input("Enter tester  name (45 char max):"))

sleep(1)
wire.write("|001008\015\012")                    # ETF
sleep(1)
wire.write("|001013\015\012")                    # RETURN
sleep(1)
wire.write("|001008\015\012")
sleep(1)
wire.write("|001008\015\012")
sleep(1)
wire.write("|001008\015\012")
sleep(1)
wire.write("|115021\015\012")                    # YYYY/MM/DD 24 hour clock
sleep(1)
wire.write("|11600%4.4s\015\012" % (mm))         # Set Month
sleep(1)
wire.write("|11601%4.4s\015\012" % (dd))         # Set Day
sleep(1)
wire.write("|11602%4.4s\015\012" % (yyyy))       # Set Year
sleep(1)
wire.write("|11700%2.2s\015\012" % (hh))         # Set Hour
sleep(1)
wire.write("|11701%2.2s\015\012" % (nn))         # Set Minute
sleep(1)
wire.write("|1050%-45.45s\015\012" % (facility)) # Set Facility
sleep(1)
wire.write("|1020%-45.45s\015\012" % (patient))  # Set Patient
sleep(1)
wire.write("|1040%-45.45s\015\012" % (tester))   # Set Tester
sleep(1)
wire.write("|1021%45.45s\015\012" % " ")
sleep(1)
print "Patient: %s" % (wire.readline())
sleep(1)
wire.write("|1200\015\012")
sleep(1)
wire.write("|1041%45.45s\015\012" % " ")
sleep(1)
print "Tester:  %s" % (wire.readline())
sleep(1)
wire.write("|1200\015\012")
sleep(1)
#wire.write("|1190001\015\012") # Transmit all tests
#sleep(1)
#
#  wire.write("|1200\015\012")

wire.close()

xlApp = Dispatch("Excel.Application")
xlApp.Workbooks.Add()
xlBook = xlApp.Workbooks(1)
xlSheet = xlApp.Sheets(1)
xlSheet.Cells(1,1).Value = patient
xlSheet.Cells(2,1).Value = tester
xlBook.SaveAs(Filename="C:\\Documents and Settings\\Wendy Hanks\\My Documents\\Code\\TympStar.xls")
xlApp.Visible = 1
