#!/usr/bin/env python
# TITLE:   Collect.py
# AUTHOR:  Kevin Cole, Gallaudet University <kjcole@gri.gallaudet.edu>
# LASTMOD: 2003.10.23
# PURPOSE: To extend the serial class to include TympStar GTI attributes
#          and methods.
# NOTES:   See "Python Programming on Win32" pages 373-379.  And note
#          that "send" is now "write".  See also "import pydoc",
#          "pydoc.doc(Serial)" for more specific info...
#
#          Further analysis leads me to suspect pySerial may be better
#          than what I've chosen below.  pySerial appears to have the
#          ability to raise and lower RTS, which means we can turn hand-
#          shake back on...
#
# Communications: COM1, 19200 baud, 2 stop bits, even parity, no handshake
#
# Set RECORD TYPE to SUMMARY or to SUMMARY + XY (graphics)
# Set CLEAR TEST to AUTO or MANUAL
# Set DATA FORMAT to GSI TYMPSTAR DATA (not GSI 33 DATA)

from TympStar import *
from win32com.client import Dispatch

DEBUG = 0              # Not debugging at the moment

log = open("TympStar.log","w")
GSI = TympStar()       # Open a line to the GSI TympStar

RT  = GSI.command["Key Press"]  # Record type
RC  = GSI.setget["Set"]         # Request code
FC  = GSI.funky["SOFTKEY6"]     # Function keycode

#for i in range(10):
#  GSI.setRTS(1)
#  #GSI.send("|001005"+CRLF)       # Send TympStar command "Menu Key 6"
#  GSI.send(SOR+RT+RC+FC+CR+LF)    # Simulate Softkey 6 press

facility = "Gallaudet University, Audiology Department"
patient = strip(raw_input("Enter patient name (45 char max):"))
tester  = strip(raw_input("Enter tester  name (45 char max):"))

# Set Transmit to Summary + XY
#
#GSI.send("001008")                     # ETF
#GSI.send("001013")                     # RETURN
#GSI.send("001002")                     # Instrument Options  (SK3)
#GSI.send("001001")                     # Data Transfer       (SK2)
#GSI.send("001000")                     # Tymp                (SK1)
#GSI.send("001001")                     # Summary + XY        (SK2)
#GSI.send("001005")                     # Store               (SK5)

# Set Facility Name
#
#GSI.send("001008")                     # ETF
#GSI.send("001013")                     # RETURN
#GSI.send("001002")                     # Instrument Options  (SK2)
#GSI.send("001005")                     # More                (SK6)
#GSI.send("001001")                     # Facility Name       (SK2)
#GSI.send("1050%-45.45s" % (facility))  # Set Facility Name
#GSI.send("001005")                     # Store               (SK5)

# Set Date and Time
#
#GSI.send("115021")                    # YYYY/MM/DD 24 hour clock
#GSI.send("11600%4.4s"   % (mm))       # Set Month
#GSI.send("11601%4.4s"   % (dd))       # Set Day
#GSI.send("11602%4.4s"   % (yyyy))     # Set Year
#GSI.send("11700%2.2s"   % (hh))       # Set Hour
#GSI.send("11701%2.2s"   % (nn))       # Set Minute

GSI.send("001006")                     # TYMP
GSI.send("1020%-45.45s" % (patient))   # Set Patient Name
GSI.send("1040%-45.45s" % (tester))    # Set Tester Name

#################################################################

if DEBUG:
  print GSI.portstr                      # Print I/O port name
  GSI.send("1021%45.45s" % " ")          # Get Patient Name
  feedback = GSI.readline()
  print "Patient:  %s" % (feedback)
  log.write(feedback)
  GSI.send("1200")                       # Acknowledge
  GSI.send("1041%45.45s" % " ")          # Get Tester Name
  feedback = GSI.readline()
  print "Tester:   %s" % (feedback)
  log.write(feedback)
  GSI.send("1200")                       # Acknowledge
  GSI.send("001008")                     # ETF
  GSI.send("001013")                     # RETURN
  GSI.send("001002")                     # Instrument Options
  GSI.send("001005")                     # More
  GSI.send("001001")                     # Facility Name
  GSI.send("1051%45.45s" % " ")          # Get Facility Name
  feedback = GSI.readline()
  print "Facility: %s" % (feedback)
  log.write(feedback)
  GSI.send("1200")                       # Acknowledge
  GSI.send("001005")                     # Store
  GSI.send("001006")                     # TYMP

#################################################################

# How do we determine how many tests are waiting?  There doesn't
# appear to be a signal from the TympStar to indicate end of trans-
# mission...  So just poll the input buffer and after seeing no input
# for some arbitrary length of time, decide we're done?  Sounds like a
# crappy way of doing things...

# Instead, ask the user how many tests to expect...

tests  = int(strip(raw_input("How many tests (1-29): ")))
GSI.send("001014")              # PAGE
GSI.send("1190001")             # Transmit all tests

for test in range(tests):
  buffer = []                     # Create an empty buffer
  done = 0                        # Not done til the fat lady sings.
  while not done:                 # Until the done flag is set...
    record = GSI.readline()       # ...Read a record
    resp   = Response(record)     # ...Check and parse it again
    while not resp.valid:         # ...Loop til we get valid record
      GSI.send("1210")            # ......Send NAK
      record = GSI.readline()     # ......Read it again
      resp   = Response(record)   # ......Check and parse it again
    GSI.send("1200")              # ...Send ACK
#   buffer.append(resp.data)      # ...Accumulate data
    buffer.append(record)
    if resp.rectype == "3":       # ...If End of XY Data...
      done = 1                    # ......We're done.
  
  for line in buffer:
    log.write(line)
#   log.write(GSI.CRLF)

  del buffer

GSI.close()
log.close()

xlApp = Dispatch("Excel.Application")
xlApp.Workbooks.Add()
xlBook = xlApp.Workbooks(1)
xlSheet = xlApp.Sheets(1)
xlSheet.Cells(1,1).Value = patient
xlSheet.Cells(2,1).Value = tester
xlBook.SaveAs(Filename="C:\\Documents and Settings\\Wendy Hanks\\My Documents\\Code\\TympStar.xls")
xlApp.Visible = 1
