#!/usr/bin/env python
from string   import *
from TympStar import *

filename = strip(raw_input("File: "))
for line in open(filename,"r").readlines():
  resp = Response(line)
  print "_"*78 + "\n"
  print "Original: %s" % (line)
  print "Start:    %s" % (resp.start)
  print "Type:     %s" % (resp.rectype)
  print "Sequence: %s" % (resp.seqnum)
  print "Query:    %s" % (resp.query)
  print "Data:     %s" % (resp.data)
  print "Sum:      %s" % (resp.sum)
  print "Check:    %s" % (resp.check)
  print "Error:    %s" % (resp.error)
  print "Valid?    %s" % (resp.valid)
  print "_"*78 + "\n"
# proceed  = strip(raw_input("Continue?"))
