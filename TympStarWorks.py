#!/usr/bin/env python
# TITLE:   TympStar.py
# AUTHOR:  Kevin Cole, Gallaudet University <kjcole@gri.gallaudet.edu>
# LASTMOD: 2003.10.23
# PURPOSE: To extend the serial class to include TympStar GTI attributes
#          and methods.
# NOTES:   See "Python Programming on Win32" pages 373-379.  And note
#          that "send" is now "write".  See also "import pydoc",
#          "pydoc.doc(Serial)" for more specific info...
#
#          Further analysis leads me to suspect pySerial may be better
#          than what I've chosen below.  pySerial appears to have the
#          ability to raise and lower RTS, which means we can turn hand-
#          shake back on...
#
# Communications: COM1, 19200 baud, 2 stop bits, even parity, no handshake
#
# Set RECORD TYPE to SUMMARY or to SUMMARY + XY (graphics)
# Set CLEAR TEST to AUTO or MANUAL
# Set DATA FORMAT to GSI TYMPSTAR DATA (not GSI 33 DATA)

from   time     import sleep
from   string   import *
from   killtime import *  # My own time routines
from   serial   import *
import serial.serialutil

# Error flags
BADSOR = 1             # Invalid Start of Record character
BADTYP = 2             # Invalid Record Type
BADSEQ = 4             # Invalid Record Sequence Number
BADSUM = 8             # Invalid Checksum

SOR = "|"              # Start of Record
TYPES = (1,2,3,4,8,9)  # Valid record types

#####################################################################################

class TympStar(Serial):
  """Commands and serial protocol for the TympStar"""

  SOR = "|"               # Start of Record
  CR  = "\015"            # Carriage Return
  LF  = "\012"            # Line Feed
  CRLF = CR+LF            # Carriage Return, Line Feed
  fmt  = SOR+"%s"+CRLF    # Standard command format

  command = {"Key Press"    : "001",
             "Patient info" : "102",
             "Tester Info"  : "104",
             "Send ACK"     : "120",
             "Send NAK"     : "121"}

  setget = {"Set" : "0", "Get" : "1"}     # Send or Query

  funky = {"SOFTKEY1" : "00", "SOFTKEY2" : "01", "SOFTKEY3" : "02",
           "SOFTKEY4" : "03", "SOFTKEY5" : "04", "SOFTKEY6" : "05",
           "TYMP"     : "06", "REFLEX"   : "07", "ETF"      : "08",
           "SPECIAL"  : "09", "START_R"  : "10", "START_L"  : "11",
           "ERASE"    : "12", "RETURN"   : "13", "PAGE"     : "14",
           "CLEAR"    : "15", "REMOTE"   : "16", "DATA_X"   : "17",
           "STIM_D"   : "18", "STIM_U"   : "19", "INTEN_D"  : "20",
           "INTEN_U"  : "21", "PRINT"    : "22", "UNUSED"   : "23",
           "STOP"     : "24", "HOLD"     : "25", "PRESENT"  : "26"}

  ACK = "|1200" + CRLF
  NAK = "|1210" + CRLF

  def __init__(self, *args, **kwargs):
    port     = kwargs.get("port",     0)
    baudrate = kwargs.get("baudrate", 19200)
    bytesize = kwargs.get("bytesize", SEVENBITS)
    stopbits = kwargs.get("stopbits", STOPBITS_TWO)
    parity   = kwargs.get("parity",   PARITY_EVEN)
    timeout  = kwargs.get("timeout",  None)

    kwargs["port"]     = port
    kwargs["baudrate"] = baudrate
    kwargs["bytesize"] = bytesize
    kwargs["stopbits"] = stopbits
    kwargs["parity"]   = parity
    kwargs["timeout"]  = timeout

    Serial.__init__(self, *args, **kwargs)

  def send(self,s):
    self.setRTS(1)
    sleep(1)
#   while self.getCTS() != 1: sleep(1)
    self.write("|%s\015\012" % (s))
    self.setRTS(0)

  def check(self,s):
    s = strip(s)                  # Remove CRLF
    data = s[:-2]                 # Everything up to the last two characters
    sent = lower("0x"+s[-2:])     # The last two characters is SENT checksum
    sum = 0
    for i in range(len(data)):
      sum += ord(data[i])         # add ASCII arithmetic value to sum
    calc = "0x%02x" % (sum % 256) # CALC-ulated checksum
    if (sent == calc) and \
       (data[0:1] == SOR):        # If checksums match and record start ok...
      integrity = 1               # ...integrity is good
#     print "Checksum OK"         # ...all is well
    else:                         # Otherwise...
      integrity = 0               # ...integrity is bad
#     print "Checksum FAILED... retrying"
    return integrity

  def get(self):
    line = self.readline()     # Get a line of data
    resp = Response(line)      # Parse out the standard parts
    if (self.check(s)):        # If the data checks okay...
      self.write(ACK)          # ...send an ACK to the TympStar
    else:                      # Otherwise...
      self.write(NAK)          # ...send an NAK to the TympStar

  def xmit(device, command, flag, args):
    sleep(1)
    fmt = SOR+"%s"+CR+LF
    device.write(fmt % (command,flag,args))
  
#####################################################################################

class Response:
  """Common characteristics of all response records"""
  def __init__(self,line):
    """Parse return string into separate fields"""
    line = strip(line)                     # Remove CRLF
    self.record  = line[:-2]               # Everything up to the last two characters
    self.start   = line[0:1]               # Start of record
    self.rectype = line[1:2]               # Record Type
    self.seqnum  = line[2:4]               # Record Sequence Number
    self.query   = None                    # Record Query Type defaults to None
    self.data    = strip(line[4:-2])       # Record Data
    self.sum     = lower("0x"+line[-2:])   # Record Checksum (hex)
    self.check   = "0x00"                  # Computed Checksum (hex)
    self.valid   = 1                       # Data Integrity defaults to valid
    self.error   = 0                       # No error code
    sum          = 0                       # Initialize computed checksum
    if int(self.rectype) == 9:             # If this was a query response...
      self.query = line[4:7]               # ...Record Query Type
      self.data  = strip(line[7:-2])       # Record Data
    for i in range(len(self.record)):      # For each character in record...
      sum += ord(self.record[i])           # ...add ASCII value to computed checksum
    self.check = "0x%02x" % (sum % 256)    # Truncate to 8-bits and hexify when done
    if (self.start != SOR):                # If record start is not "|"...
      self.error = self.error | BADSOR     # ...Invalid SOR
    if int(self.rectype) not in TYPES:     # If invalid record type...
      self.error = self.error | BADTYP     # ...Invalid record type
    if self.check != self.sum:             # If checksums don't match
      self.error = self.error | BADSUM     # ...Invalid checksum
    if self.error:                         # If any error bits set...
      self.valid = 0                       # ...Record Data Integrity is invalid

#####################################################################################

class TympDiag:
  """Tymp Diagnostic"""
  def __init__(self,line):
    """Parse return string into separate fields"""
    line = strip(line)                       # Remove CRLF
    self.record  = line[:-2]                 # Everything up to the last two characters
    self.start   = line[0:1]                 # Start of record
    self.rectype = line[1:2]                 # Record Type
    self.seqnum  = line[2:4]                 # Record Sequence Number
    self.query   = None                      # Record Query Type defaults to None
    self.data    = strip(line[4:-2])         # Record Data
    self.sum     = lower("0x"+line[-2:])     # Record Checksum (hex)
    self.valid   = 1                         # Data Integrity defaults to valid
    self.error   = 0                         # No error code
    sum          = 0                         # Initialize computed checksum
    if self.rectype == "9":                  # If this was a query response...
      self.query = line[4:7]                 # ...Record Query Type
      self.data  = strip(line[7:-2])         # Record Data
    for i in range(len(self.record)):        # For each character in record...
      sum += ord(self.record[i])             # ...add ASCII value to computed checksum
    self.check = "0x%02x" % (sum % 256)      # Truncate to 8-bits and hexify when done
    if (self.start != SOR):                  # If record start is not "|"...
      self.error = self.error | BADSOR       # ...Invalid SOR
    if int(self.rectype) not in TYPES:       # If invalid record type...
      self.error = self.error | BADTYP       # ...Invalid record type
    if self.check != self.sum:               # If checksums don't match
      self.error = self.error | BADSUM       # ...Invalid checksum
    if self.error:                           # If any error bits set...
      self.valid = 0                         # ...Record Data Integrity is invalid

    self.patientName  = strip(line[4:49])
    self.patientID    = strip(line[49:94])
    self.testerName   = strip(line[94:139])
    self.facility     = strip(line[139:184])
    self.probeSN      = strip(line[184:193])
    self.dateTime     = strip(line[193:213])   # 99/99/99 99:99 [ap]m
    self.testType     = strip(line[213:215])   # 0=Tymp Diagnostic
    self.testNumber   = strip(line[215:216])   # 1-9 or A-Q
    self.earTesting   = strip(line[216:217])   # R or L
    self.autoSeq      = strip(line[217:218])   # 0=Off, 1=On
    self.probeTone    = strip(line[218:220])   # 0=226 Hz, 1=678 Hz, 2= 1000 Hz
    self.sPressure    = strip(line[220:224])   # -600 to +400 daPa
    self.baseline     = strip(line[224:225])   # 0=Off, 1=On
    self.gradient     = strip(line[225:226])   # 0=Off, 1=Tymp Width daPa, 2=Ratio ml
    self.ECVCI        = strip(line[226:232])   # -30800 to +30800
    self.numLines     = strip(line[232:233])   # 0, 1, 2, 3
    self.Yscale       = strip(line[233:234])   # Y axis scale
    self.Xvalue       = strip(line[234:239])   # Cursor X value

    self.L1Header     = strip(line[239:241])   # Line 1 data header
    self.L1Yvalue     = strip(line[241:247])   # Line 1 cursor Y value
    self.L1Admittance = strip(line[247:248])   # 0=Y, 1=B, 2=G, 3=B/G
    self.L1Range      = strip(line[248:249])   # 0=Normal, 1=Wide
    self.L1Rate       = strip(line[249:250])   # 0=12.5, 1=50, 2=200, 3=600/200
    self.L1Compliance = strip(line[250:256])   # -30800 to +30800
    self.L1Pressure   = strip(line[256:260])   # -600 to +400 daPa
    self.L1Direction  = strip(line[260:261])   # 0=plus-to-minus, 1=minus-to-plus
    self.L1Gradient   = strip(line[261:264])   # Depends on gradient above

    self.L2Header     = strip(line[264:266])   # Line 2 data header
    self.L2Yvalue     = strip(line[266:272])   # Line 2 cursor Y value
    self.L2Admittance = strip(line[272:273])   # 0=Y, 1=B, 2=G, 3=B/G
    self.L2Range      = strip(line[273:274])   # 0=Normal, 1=Wide
    self.L2Rate       = strip(line[274:275])   # 0=12.5, 1=50, 2=200, 3=600/200
    self.L2Compliance = strip(line[275:281])   # -30800 to +30800
    self.L2Pressure   = strip(line[281:285])   # -600 to +400 daPa
    self.L2Direction  = strip(line[285:286])   # 0=plus-to-minus, 1=minus-to-plus
    self.L2Gradient   = strip(line[286:289])   # Depends on gradient above

    self.L3Header     = strip(line[289:291])   # Line 3 data header
    self.L3Yvalue     = strip(line[291:297])   # Line 3 cursor Y value
    self.L3Admittance = strip(line[297:298])   # 0=Y, 1=B, 2=G, 3=B/G
    self.L3Range      = strip(line[298:299])   # 0=Normal, 1=Wide
    self.L3Rate       = strip(line[299:300])   # 0=12.5, 1=50, 2=200, 3=600/200
    self.L3Compliance = strip(line[300:306])   # -30800 to +30800
    self.L3Pressure   = strip(line[306:310])   # -600 to +400 daPa
    self.L3Direction  = strip(line[310:311])   # 0=plus-to-minus, 1=minus-to-plus
    self.L3Gradient   = strip(line[311:314])   # Depends on gradient above

#####################################################################################

class XYdata:
  """XY coordinate pairs for graphical data"""
  def __init__(self,line):
    """Parse return string into separate fields"""
    line = strip(line)                     # Remove CRLF
    self.record  = line[:-2]               # Everything up to the last two characters
    self.start   = line[0:1]               # Start of record
    self.rectype = line[1:2]               # Record Type 2=XY Graphical Data
    self.seqnum  = line[2:4]               # Record Sequence Number
    self.query   = None                    # Record Query Type defaults to None
    self.data    = strip(line[4:-2])       # Record Data
    self.sum     = lower("0x"+line[-2:])   # Record Checksum (hex)
    self.valid   = 1                       # Data Integrity defaults to valid
    self.error   = 0                       # No error code
    sum          = 0                       # Initialize computed checksum
    if self.rectype == "9":                # If this was a query response...
      self.query = line[4:7]               # ...Record Query Type
      self.data  = strip(line[7:-2])       # Record Data
    for i in range(len(self.record)):      # For each character in record...
      sum += ord(self.record[i])           # ...add ASCII value to computed checksum
    self.check = "0x%02x" % (sum % 256)    # Truncate to 8-bits and hexify when done
    if (self.start != SOR):                # If record start is not "|"...
      self.error = self.error | BADSOR     # ...Invalid SOR
    if int(self.rectype) not in TYPES:     # If invalid record type...
      self.error = self.error | BADTYP     # ...Invalid record type
    if self.check != self.sum:             # If checksums don't match
      self.error = self.error | BADSUM     # ...Invalid checksum
    if self.error:                         # If any error bits set...
      self.valid = 0                       # ...Record Data Integrity is invalid

    self.testNumber   = strip(line[4:5])    # 1-9 or A-Q
    self.L1NumPairs   = strip(line[5:8])    # Number of XY pairs for Line 1
    self.L2NumPairs   = strip(line[8:11])   # Number of XY pairs for Line 2
    self.L3NumPairs   = strip(line[11:14])  # Number of XY pairs for Line 3

    self.L1XY = Array()
    self.L2XY = Array()
    self.L3XY = Array()

# The following section needs serious work: Not only do we have the
# wrong hex function (it's the inverse of what we want) but also the
# number of pairs in a single record cannot exceed 29.  So, if there
# are 28 pairs for line 1, then line 2 begins with the last pair on
# the first record and continues to the next record, where the first
# pair in the record will be the second pair of Line 2.

# New thought: The correct way to do this is to read everything
# into a buffer.  The buffer should start with the Tymp Diagnostic
# record, followed by N graphical data records, and terminated
# by an End of XY data record...  THEN parse everything out.

    offset = 14
    for pair in range(int(L1NumPairs)):
      # We need a check for valid  4-digit hex...
      X = hex(strip(line[offset:offset+4]))
      Y = hex(strip(line[offset+4:offset+8]))
      self.L1XY.append([X,Y])
      offset = offset+8
    for pair in range(int(L2NumPairs)):
      # We need a check for valid  4-digit hex...
      X = hex(strip(line[offset:offset+4]))
      Y = hex(strip(line[offset+4:offset+8]))
      self.L2XY.append([X,Y])
      offset = offset+8
    for pair in range(int(L3NumPairs)):
      # We need a check for valid  4-digit hex...
      X = hex(strip(line[offset:offset+4]))
      Y = hex(strip(line[offset+4:offset+8]))
      self.L3XY.append([X,Y])
      offset = offset+8

#####################################################################################

class EndXY:
  """End of summary and XY data"""
  def __init__(self,line):
    """Parse return string into separate fields"""
    line = strip(line)                       # Remove CRLF
    self.record  = line[:-2]                 # Everything up to the last two characters
    self.start   = line[0:1]                 # Start of record
    self.rectype = line[1:2]                 # Record Type 3=End of Summary and XY Data
    self.seqnum  = line[2:4]                 # Record Sequence Number
    self.query   = None                      # Record Query Type defaults to None
    self.data    = strip(line[4:-2])         # Record Data
    self.sum     = lower("0x"+line[-2:])     # Record Checksum (hex)
    self.valid   = 1                         # Data Integrity defaults to valid
    self.error   = 0                         # No error code
    sum          = 0                         # Initialize computed checksum
    if self.rectype == "9":                  # If this was a query response...
      self.query = line[4:7]                 # ...Record Query Type
      self.data  = strip(line[7:-2])         # Record Data
    for i in range(len(self.record)):        # For each character in record...
      sum += ord(self.record[i])             # ...add ASCII value to computed checksum
    self.check = "0x%02x" % (sum % 256)      # Truncate to 8-bits and hexify when done
    if (self.start != SOR):                  # If record start is not "|"...
      self.error = self.error | BADSOR       # ...Invalid SOR
    if int(self.rectype) not in TYPES:       # If invalid record type...
      self.error = self.error | BADTYP       # ...Invalid record type
    if self.check != self.sum:               # If checksums don't match
      self.error = self.error | BADSUM       # ...Invalid checksum
    if self.error:                           # If any error bits set...
      self.valid = 0                         # ...Record Data Integrity is invalid

class Error:
  """Error return"""
  def __init__(self,line):
    """Parse return string into separate fields"""
    line = strip(line)                       # Remove CRLF
    self.record  = line[:-2]                 # Everything up to the last two characters
    self.start   = line[0:1]                 # Start of record
    self.rectype = line[1:2]                 # Record Type
    self.seqnum  = line[2:4]                 # Record Sequence Number
    self.query   = None                      # Record Query Type defaults to None
    self.data    = strip(line[4:-2])         # Record Data
    self.sum     = lower("0x"+line[-2:])     # Record Checksum (hex)
    self.valid   = 1                         # Data Integrity defaults to valid
    self.error   = 0                         # No error code
    sum          = 0                         # Initialize computed checksum
    if self.rectype == "9":                  # If this was a query response...
      self.query = line[4:7]                 # ...Record Query Type
      self.data  = strip(line[7:-2])         # Record Data
    for i in range(len(self.record)):        # For each character in record...
      sum += ord(self.record[i])             # ...add ASCII value to computed checksum
    self.check = "0x%02x" % (sum % 256)      # Truncate to 8-bits and hexify when done
    if (self.start != SOR):                  # If record start is not "|"...
      self.error = self.error | BADSOR       # ...Invalid SOR
    if int(self.rectype) not in TYPES:       # If invalid record type...
      self.error = self.error | BADTYP       # ...Invalid record type
    if self.check != self.sum:               # If checksums don't match
      self.error = self.error | BADSUM       # ...Invalid checksum
    if self.error:                           # If any error bits set...
      self.valid = 0                         # ...Record Data Integrity is invalid

    self.code    = strip(line[4:7])          # Error code
    self.subcode = strip(line[7:11])         # Error subcode
