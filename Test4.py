#!/usr/bin/env python
from string   import *
from TympStar import *

filename = strip(raw_input("File: "))
log = open(filename,"r")

buffer = []                   # Create an empty buffer
done = 0

while not done:               # Until the done flag is set...
  record = log.readline()     # ...Read a record
  resp   = Response(record)   # ...Check and parse it again
  while not resp.valid:       # ...Loop til we get valid record
    print "NAK"               # ......Send NAK
    record = log.readline()   # ......Read it again
    resp   = Response(record) # ......Check and parse it again
  print "ACK"                 # ...Send ACK
  buffer.append(resp.data)    # ...Accumulate data
  if resp.rectype == "3":     # ...If End of XY Data...
    done = 1                  # ......We're done

print "="*72
for line in buffer:
  print line
print "="*72

log.close()
